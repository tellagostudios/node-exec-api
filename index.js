/*jslint nomen: true*/
var winston         = require("winston");
var baseConnector   = require('kido-connector');

baseConnector.init('Exec', winston);

var ExecConnector = function (config) {
    "use strict";

    winston.debug("Initializating ...");
    if (!config || typeof config !== 'object') { throw new Error("Configuration is missing or invalid."); }

    winston.debug("Initialized.");

    this.exec = function (data, cb) {
        if (!data) { return cb(new Error("data object is mandatory")); }
        if (!data.cmd) { return cb(new Error("data.cmd property is mandatory")); }

        var cmd = data.cmd,
            args = data.args || [""],
            options = data.options,
            childStdOutData = "",
            childStdErrData = "",

            spawn = require('child_process').spawn,
            child = spawn(cmd, args, options);

        if (data.timeout) {
            setTimeout(function killWhenTimeout() {
                winston.info("Killing process: " + child.pid);
                child.kill(data.signal || "SIGKILL");

            }, data.timeout);
        }
        child.stdout.on('data', function (buffer) {
            childStdOutData += buffer;
        });

        child.stderr.on('data', function (buffer) {
            childStdErrData += buffer;
        });

        child.on('exit', function (code, signal) {
            winston.debug("Child Exiting");

            //We have to make an explicit check for undefined
            //because the number 0 is a valid value. We can't use the OR (||) operator
            if (code === undefined) {
                code = null;
            }
            return cb(null, {code: code, signal: signal, stdout: childStdOutData || "", stderr: childStdErrData || ""});
        });

    };
};

module.exports = ExecConnector;