/*global describe, beforeEach, before, it */
/*jslint nomen: true*/

var assert        = require("assert");
var Exec          = require("../index.js");
var winston       = require("winston");

winston.clear();
//winston.add(winston.transports.Console, { colorize: true, level: 'debug' });

describe("Exec connector", function () {
    "use strict";

    var config = {};

    describe("constructor", function () {
        it("should fail if not configuration", function () {
            try {
                var exec = new Exec();
                assert.ok(!exec, "Exception was not throw");
            } catch (err) {
                assert.ok(err instanceof Error);
                assert.equal("Configuration is missing or invalid.", err.message);
            }
        });

        it("should be able to create an instance", function () {
            var exec = new Exec(config);
            assert.ok(exec instanceof Exec);
        });
    });

    describe("Executing", function () {
        it("should fail if invalid data", function (done) {
            var data = null,
                exec = new Exec(config);

            assert.ok(exec instanceof Exec);
            exec.exec(data, function (err, output) {
                assert.ok(err);
                assert.ok(!output);
                assert.strictEqual(err.message, "data object is mandatory");

                done();
            });
        });

        it("should fail if invalid command", function (done) {
            var data = {},
                exec = new Exec(config);

            assert.ok(exec instanceof Exec);
            exec.exec(data, function (err, output) {
                assert.ok(err);
                assert.ok(!output);
                assert.strictEqual(err.message, "data.cmd property is mandatory");

                done();
            });
        });

        it("should not execute command", function (done) {
            var data = {
                    cmd: "invalid",
                    args: ["-an"]
                },
                exec = new Exec(config);

            assert.ok(exec instanceof Exec);
            exec.exec(data, function (err, output) {
                assert.ok(!err);
                assert.ok(output);
                assert.ok(output.code !== 0);

                done();
            });
        });

        it("should execute command ok", function (done) {
            var data,
                exec = new Exec(config);

            if (process.platform === "win32") {
                data = {
                    cmd: "netstat.exe",
                    args: ["-an"]
                };
            } else {
                data = {
                    cmd: "ls",
                    args: ["-ltr", "/tmp/"]
                };
            }

            assert.ok(exec instanceof Exec);
            exec.exec(data, function (err, output) {
                assert.ok(!err);
                assert.ok(output);
                assert.strictEqual(output.code, 0);
                assert.ok(output.stdout);
                assert.ok(output.stdout.length > 0);
                assert.strictEqual(output.stderr, "");

                done();
            });
        });

        it("should timeout command", function (done) {
            var data,
                exec = new Exec(config);

            if (process.platform === "win32") {
                data = {
                    cmd: "notepad.exe",
                    args: [""]
                };
            } else {
                data = {
                    cmd: "gedit",
                    args: [""],
                    timeout: 1000
                };
            }

            assert.ok(exec instanceof Exec);
            exec.exec(data, function (err, output) {
                assert.ok(!err);

                assert.ok(output);
                assert.strictEqual(output.signal, "SIGKILL");

                done();
            });
        });
    });
});